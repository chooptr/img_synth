#ifndef TPS_BOUNDINGBOX_H
#define TPS_BOUNDINGBOX_H


#include "vector3.h"
#include "world.h"
#include "ray.h"

struct BoundingBox {
    VECF bounds[2];

    [[nodiscard]] virtual std::optional<TraceInfo*> get_intersection_point(const Ray& r) const = 0;

    virtual void print(std::ostream& str) const = 0;

    friend std::ostream& operator<<(std::ostream& str, BoundingBox const& data) {
        data.print(str);
        return str;
    }
};

struct BoundingBoxNode : public BoundingBox {
    BoundingBox* left, * right;

    BoundingBoxNode(BoundingBox* left, BoundingBox* right);

    void print(std::ostream& str) const override;

    [[nodiscard]] std::optional<TraceInfo*> get_intersection_point(const Ray& r) const override;

	[[nodiscard]] bool does_intersect(const Ray& r) const;
};

struct BoundingBoxLeaf : public BoundingBox {
    const Sphere* single_object;

    explicit BoundingBoxLeaf(const Sphere* sphere);

    void print(std::ostream& str) const override;

    [[nodiscard]] std::optional<TraceInfo*> get_intersection_point(const Ray& r) const override;
};

BoundingBox* create_box(const std::vector<Sphere>& vec, int lo=0, int hi=-1);

BoundingBox* create_box_bsp(const std::vector<Sphere>& vec);

#endif //TPS_BOUNDINGBOX_H
