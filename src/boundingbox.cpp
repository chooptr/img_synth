#ifndef TPS_BOUNDINGBOX_CPP
#define TPS_BOUNDINGBOX_CPP

#include "vector3.h"
#include "world.h"
#include "boundingbox.h"

// BoundingBox
BoundingBox* create_box(const std::vector<Sphere>& vec, int lo, int hi) {
	if (hi == lo + 1) return new BoundingBoxLeaf(&vec[lo]);
	if (hi == -1) hi = vec.size();

	BoundingBox* left = create_box(vec, lo, lo + (hi - lo) / 2);
	BoundingBox* right = create_box(vec, lo + (hi - lo) / 2, hi);

	return new BoundingBoxNode(left, right);
}

BoundingBox* create_box_bsp(const std::vector<Sphere>& vec) {
	if (vec.size() == 1) {
		return new BoundingBoxLeaf(&vec[0]);
	}

	if (vec.size() == 2) {
		return new BoundingBoxNode(
				new BoundingBoxLeaf(&vec[0]),
				new BoundingBoxLeaf(&vec[1])
		);
	}

	auto s0 = vec[0];
	VECF min = s0.get_min_bounds();
	VECF max = s0.get_max_bounds();

	for (const auto & i : vec) {
		auto s_min = i.get_min_bounds();
		min.x = std::min(s_min.x, min.x);
		min.y = std::min(s_min.y, min.y);
		min.z = std::min(s_min.z, min.z);

		auto s_max = i.get_max_bounds();
		max.x = std::max(s_max.x, max.x);
		max.y = std::max(s_max.y, max.y);
		max.z = std::max(s_max.z, max.z);
	}

	float dx = max.x - min.x;
	float dy = max.y - min.y;
	float dz = max.z - min.z;

	auto* vec_left = new std::vector<Sphere>();
	auto* vec_right = new std::vector<Sphere>();

	if (dx > dy && dx > dz) {
		for (auto sphere : vec) {
				if (sphere.center.x > (dx / 2.f + min.x)) {
				vec_right->push_back(sphere);
			} else vec_left->push_back(sphere);
		}
	} else if (dy > dx && dy > dz) {
		for (auto sphere : vec) {
				if (sphere.center.y > (dy / 2.f + min.y)) {
				vec_right->push_back(sphere);
			} else vec_left->push_back(sphere);
		}
	} else {
		for (auto sphere : vec) {
				if (sphere.center.z > (dz / 2.f + min.z)) {
				vec_right->push_back(sphere);
			} else vec_left->push_back(sphere);
		}
	}

	BoundingBox* b1 = create_box_bsp(*vec_left);
	BoundingBox* b2 = create_box_bsp(*vec_right);
	return new BoundingBoxNode(b1, b2);
}

BoundingBoxNode::BoundingBoxNode(BoundingBox* left, BoundingBox* right) : left(left), right(right) {
	bounds[0].x = std::min(left->bounds[0].x, right->bounds[0].x);
	bounds[0].y = std::min(left->bounds[0].y, right->bounds[0].y);
	bounds[0].z = std::min(left->bounds[0].z, right->bounds[0].z);

	bounds[1].x = std::max(left->bounds[1].x, right->bounds[1].x);
	bounds[1].y = std::max(left->bounds[1].y, right->bounds[1].y);
	bounds[1].z = std::max(left->bounds[1].z, right->bounds[1].z);
}

void BoundingBoxNode::print(std::ostream& str) const {
	str << "Bounds:(" << bounds[0] << "," << bounds[1] << "\n\tChildren(" << *left << "   " << *right << ")";
}

bool BoundingBoxNode::does_intersect(const Ray& r) const {
	float tmin, tmax, tymin, tymax, tzmin, tzmax;

	tmin = (bounds[r.sign[0]].x - r.origin.x) * r.invdir.x;
	tmax = (bounds[1 - r.sign[0]].x - r.origin.x) * r.invdir.x;
	tymin = (bounds[r.sign[1]].y - r.origin.y) * r.invdir.y;
	tymax = (bounds[1 - r.sign[1]].y - r.origin.y) * r.invdir.y;

	if ((tmin > tymax) || (tymin > tmax))
		return false;
	if (tymin > tmin)
		tmin = tymin;
	if (tymax < tmax)
		tmax = tymax;

	tzmin = (bounds[r.sign[2]].z - r.origin.z) * r.invdir.z;
	tzmax = (bounds[1 - r.sign[2]].z - r.origin.z) * r.invdir.z;

	return !((tmin > tzmax) || (tzmin > tmax));
}

[[nodiscard]] std::optional<TraceInfo*> BoundingBoxNode::get_intersection_point(const Ray& r) const {
	if (!does_intersect(r)) return std::nullopt;

	auto inter_left = left->get_intersection_point(r);
	auto inter_right = right->get_intersection_point(r);

	if (inter_left.has_value()) {
		if (inter_right.has_value()) {
			if (inter_left.value()->distance < inter_right.value()->distance)
				return inter_left;
			return inter_right;
		}
		return inter_left;
	}
	if (inter_right.has_value()) {
		return inter_right;
	}
	return std::nullopt;
}


// BoundingBoxLeaf
BoundingBoxLeaf::BoundingBoxLeaf(const Sphere* sphere) : single_object(sphere) {
	bounds[0] = sphere->get_min_bounds();
	bounds[1] = sphere->get_max_bounds();
}

void BoundingBoxLeaf::print(std::ostream& str) const {
	str << "LEAF(" << bounds[0] << "," << bounds[1] << ")";
}

[[nodiscard]] std::optional<TraceInfo*> BoundingBoxLeaf::get_intersection_point(const Ray& r) const {
	auto pt = r.get_intersection_point(single_object);
	if (pt.has_value()) {
		return new TraceInfo{single_object, pt.value().first, pt.value().second};
	}
	return std::nullopt;
}

#endif //TPS_BOUNDINGBOX_CPP
