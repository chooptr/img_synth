#ifndef TPS_RAY_H
#define TPS_RAY_H

#include "vector3.h"
#include "world.h"


struct TraceInfo {
    const Sphere* sphere = nullptr;
    VECF point;
    float distance = 0;
};

struct Ray {
    Ray() = default;

    Ray(const VECF& origin, const VECF& direction) : origin(origin), direction(direction) {
        invdir = 1.f / direction;
        sign[0] = (invdir.x < 0);
        sign[1] = (invdir.y < 0);
        sign[2] = (invdir.z < 0);
    }

    VECF origin, direction;
    VECF invdir;
    bool sign[3]{};

    [[nodiscard]] std::optional<VECF> ray_trace() const;
    [[nodiscard]] std::optional<TraceInfo*> get_closest_object() const;
    inline std::optional<float> get_intersection_distance(const Sphere* s) const;
    std::optional<std::pair<VECF, float>> get_intersection_point(const Sphere* s) const;
};
#endif //TPS_RAY_H
