#include <iostream>
#include <chrono>
#include <boost/random.hpp>

#include "world.h"
#include "ray.h"
#include "../lib/img/ppm.h"
#include "boundingbox.h"

using namespace std::chrono;

const int SAMPLE_SIZE = 10;
const float SAMPLER = 1.f / SAMPLE_SIZE;

int main() {
    auto spheres = std::vector<Sphere>();

    for (int i = 0; i < 100; ++i) {
        VECF pos = VECF{GEN(RNG) * 600, GEN(RNG) * 600, GEN(RNG) * 600};
        float radius = GEN(RNG) * 60;
        VECF col = VECF{GEN(RNG), GEN(RNG), GEN(RNG)};
        world_objects.spheres.push_back(Sphere {pos, radius, col});
    }

    auto box = create_box_bsp(world_objects.spheres);

    for (size_t i = 0; i < spheres.size(); ++i) {
       world_objects.spheres[i] = spheres[i];
    }
    world_objects.box = box;

    std::cout << "Done initializing\n";

    const VECF FOCAL_PT{300, 300, -3500};
    VECF** cam = new VECF* [600];

    for (int i = 0; i < 600; ++i) {
        cam[i] = new VECF[600]{};
        for (int j = 0; j < 600; ++j) {
            auto c = VECF{(float) i, (float) j, 0.f};
            cam[i][j] = c;
        }
    }
    auto start = high_resolution_clock::now();
    PPM ppm(600, 600, 255);

#pragma omp parallel for default(none) shared(ppm, cam, world_objects, lights, FOCAL_PT, SAMPLE_SIZE, SAMPLER) private(RNG, GEN)
    for (int i = 0; i < 600; ++i) {
        for (int j = 0; j < 600; ++j) {
            auto colour = VECF{0, 0, 0};
            ppm.pixelMatrix[i][j] = VECI(0, 0, 0);
            bool nocolor = true;
            for (int s = 0; s < SAMPLE_SIZE; ++s) {
                auto pt = VECF{GEN(RNG), GEN(RNG), 0} + cam[i][j];
                auto res = Ray(pt, (pt - FOCAL_PT).normalized()).ray_trace();
                if (res.has_value()) nocolor = false;
                colour = colour + res.value_or(VECF{0, 0, 0});
            }

            if (!nocolor) colour = SAMPLER * colour;
            else colour = VECF{0.35, 0.35, 0.35};
            ppm.pixelMatrix[i][j] = VECI{
                    std::clamp<int>((int) (colour.x * 255), 0, 255),
                    std::clamp<int>((int) (colour.y * 255), 0, 255),
                    std::clamp<int>((int) (colour.z * 255), 0, 255)
            };
        }
    }

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);
    std::cout << "Raytracing duration: " << duration.count() << '\n';
    ppm.save("wip_bb.ppm");

    return 0;
}