#include "world.h"
#include "ray.h"
#include "boundingbox.h"


inline std::optional<float> Ray::get_intersection_distance(const Sphere* s) const {
    float b = 2 * (origin.dot(direction) - s->center.dot(direction));
    float c;
    c = origin.squared_norm() + s->center.squared_norm() - 2 * s->center.dot(origin) - (s->radius * s->radius);
    float delta = b * b - 4 * c;

    if (delta < 0) return std::nullopt;

    float sol1 = (-b - std::sqrt(delta)) / 2.f;
    if (sol1 > 0.1f) return sol1;

    float sol2 = (-b + std::sqrt(delta)) / 2.f;
    if (sol2 > 0.1f) return sol2;

    return std::nullopt;
}

std::optional<std::pair<VECF, float>> Ray::get_intersection_point(const Sphere* s) const {
    auto res = get_intersection_distance(s);
    if (res) {
        auto v = res.value();

        if (v >= 0.1) return std::pair(origin + (v * direction), v);
    }

    return std::nullopt;
}

std::optional<TraceInfo*> Ray::get_closest_object() const {
//    float min_dist = std::numeric_limits<float>::max();
//    std::optional<const Sphere*> closest = std::nullopt;
//    std::optional<std::pair<VECF, float>> inter;
//    std::optional<VECF> inter_point = std::nullopt;
//
//    for (auto& world_object : world_objects.spheres) {
//        inter = get_intersection_point(&world_object);
//        if (!inter) continue;
//        auto inter_dist = inter.value().second;
//        if (inter_dist < min_dist) {
//            min_dist = inter_dist;
//            inter_point = inter.value().first;
//            closest = &world_object;
//        }
//    }
//
//    if (closest) return new TraceInfo{closest.value(), inter_point.value(), min_dist};
//
//    return std::nullopt;
    return world_objects.box->get_intersection_point(*this);
}

std::optional<VECF> Ray::ray_trace() const {
    VECF colour = VECF{0.f, 0.f, 0.f};

    auto closest = get_closest_object();
    if (!closest.has_value()) {
        return std::nullopt;
    }

    TraceInfo* info = closest.value();
    const Sphere* sphere = info->sphere;
    VECF inter = info->point;
    float distance_closest = info->distance;

    { // surface lights
        for (auto& light : lights) {
            VECF point_o = light.center;
            for (int i = 0; i < light.points; ++i) {
                VECF point_n = 2.f * float(i) * VECF{GEN(RNG), GEN(RNG), 0} + point_o;
                Ray ray_light = Ray(inter, (point_n - inter).normalized());

                { // shadows
//                    bool no_collision = true;
//                    for (const auto& s : world_objects.spheres) {
//                        std::optional<float> distance_to_obj = ray_light.get_intersection_distance(&s);
//                        if (distance_to_obj && distance_to_obj < distance_closest) {
//                            no_collision = false;
//                            break;
//                        }
//                    }
                    if (!ray_light.get_closest_object()) {
                        float dist_light = inter.distance(light.center);
                        VECF normal = (inter - sphere->center).normalized();

                        colour = light.intensity
                                 * std::max(0.f, normal.dot(ray_light.direction))
                                 / (dist_light * dist_light)
                                 * light.colour
                                 + colour;
                    }
                }
            }
        }
    }

    return colour * sphere->colour;
}

