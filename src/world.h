#ifndef TPS_WORLD_H
#define TPS_WORLD_H

#include <boost/random.hpp>

#include "vector3.h"

struct BoundingBox;

struct Sphere {
    VECF center;
    float radius = 1.f;
    VECF colour;
//    Material mat;

    [[nodiscard]] VECF get_min_bounds() const;
    [[nodiscard]] VECF get_max_bounds() const;
};

struct Light : public Sphere {
    float intensity = 0;
    int points = 50;
};

static boost::random::mt19937 RNG;

static boost::random::uniform_real_distribution<double> GEN(0.0, 1.0);

struct WorldObjects {
    std::vector<Sphere> spheres;
    BoundingBox* box = nullptr;
};

extern WorldObjects world_objects;

extern std::vector<Light> const lights;

#endif //TPS_WORLD_H
