#ifndef IMAGE_SYNTHESIS_VEC3_H
#define IMAGE_SYNTHESIS_VEC3_H

#include <iostream>
#include <cmath>
#include <algorithm>

template<typename T>
struct Vector3 {
    T x, y, z;

    Vector3() {
        x = 0;
        y = 0;
        z = 0;
    }

    Vector3(const T &x, const T &y, const T &z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    Vector3 normalized() {
        float norm_ = norm();
        return Vector3{
                x / norm_,
                y / norm_,
                z / norm_
        };
    }

    float distance(const Vector3 &rhs) const {
        return sqrt((x - rhs.x) * (x - rhs.x)
                    + (y - rhs.y) * (y - rhs.y)
                    + (z - rhs.z) * (z - rhs.z));
    }

    float dot(const Vector3 &rhs) const {
        return x * rhs.x + y * rhs.y + z * rhs.z;
    }

    [[nodiscard]] float squared_norm() const {
        return this->dot(*this);
    }

    [[nodiscard]] float norm() const {
        return sqrt(this->squared_norm());
    }

    bool operator==(const Vector3 &rhs) const {
        return x == rhs.x &&
               y == rhs.y &&
               z == rhs.z;
    }

    bool operator!=(const Vector3 &rhs) const {
        return !(rhs == *this);
    }

    Vector3<T> operator+(const Vector3 &rhs) const {
        return Vector3<T>{x + rhs.x, y + rhs.y, z + rhs.z};
    }

    Vector3<T> operator-(const Vector3 &rhs) const {
        return Vector3<T>{x - rhs.x, y - rhs.y, z - rhs.z};
    }

    Vector3<T> operator*(const Vector3 &rhs) const {
        return Vector3<T>{x * rhs.x, y * rhs.y, z * rhs.z};
    }

    Vector3<T> operator/(const Vector3 &rhs) const {
        return Vector3<T>{x / rhs.x, y / rhs.y, z / rhs.z};
    }

    friend Vector3<T> operator*(const float &scalar, const Vector3<T> &rhs) {
        return Vector3{(T)(scalar * rhs.x), (T)(scalar * rhs.y), (T)(scalar * rhs.z)};
    }

	friend Vector3<T> operator+(const float &scalar, const Vector3<T> &vector3) {
		return Vector3{(T)(scalar + vector3.x), (T)(scalar + vector3.y), (T)(scalar + vector3.z)};
	}

	friend Vector3<T> operator-(const float &scalar, const Vector3<T> &vector3) {
		return Vector3{(T)(scalar - vector3.x), (T)(scalar - vector3.y), (T)(scalar - vector3.z)};
	}

	friend Vector3<T> operator/(const float &scalar, const Vector3<T> &vector3) {
		return Vector3{(T)(scalar / vector3.x), (T)(scalar / vector3.y), (T)(scalar / vector3.z)};
	}

    friend std::ostream &operator<<(std::ostream &os, const Vector3<T> &vector3) {
        return os << "[" << vector3.x << ", " << vector3.y << ", " << vector3.z << "]";
    }
};

using VECF = Vector3<float>;
using VECI = Vector3<int>;

#endif