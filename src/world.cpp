#include "world.h"
#include "boundingbox.h"

VECF Sphere::get_min_bounds() const {
    return -radius + center;
}

VECF Sphere::get_max_bounds() const {
    return radius + center;
}

//std::vector<Sphere> spheres = {
//        {VECF{210, 165, 1100}, 35, {0.10, 0.10, 0.70}},
//		{VECF{170, 120, 1230}, 60, {0.66, 0.9, 0.9}},
//		{VECF{250, 350, 1500}, 60, {0.44, 0.55, 0.14}},
//		{VECF{0, 0, 1150}, 30, {0.4, 0.2, 0.14}},
//		{VECF{500, 350, 1100}, 20, {0.5, 0.55, 0.5}},
//		{VECF{350, 500, 1100}, 25, {0.10, 0.10, 0.70}},
//		{VECF{50, 400, 1150}, 30, {0.80, 0.40, 0.14}},
//        {VECF{300, 300, 5000}, 3500, {0.3, 0.3, 0.3}}
//};


WorldObjects world_objects = {
        std::vector<Sphere>(),
        nullptr
};

std::vector<Light> const lights = {
		{VECF{300, 300, 0}, 1, {0.f, 0.f, 1.f}, 5000.f, 100},
		{VECF{400, 450, 50}, 1, {1.f, 0.f, 0.f}, 5000.f, 100}
};
